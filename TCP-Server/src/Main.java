
public class Main {
    public static void main(String args[]) {
        if(args.length != 0) {
            App serverApp = new App(args);
            serverApp.start();
        } else {
            System.out.println("ERROR: The command line is empty");
        }
    }
}
