

public class Main {
    public static void main(String[] args) {
        if(args.length != 0) {
            App clientApp = new App(args);
            clientApp.start();
        } else {
            System.out.println("ERROR: The command line is empty");
        }
    }
}
