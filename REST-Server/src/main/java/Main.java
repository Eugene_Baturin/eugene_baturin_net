import io.undertow.Handlers;
import io.undertow.Undertow;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class Main {
    private static Map nameUsers;
    private static Map tokenUsers;
    private static List messages;
    private static TimeoutManager timeoutManager;

    static {
        messages = new ArrayList<Message>();
        tokenUsers = new ConcurrentHashMap<UUID, User>();
        nameUsers = new ConcurrentHashMap<String, User>();
        timeoutManager = new TimeoutManager(nameUsers, tokenUsers, messages);
    }

    public static void main(final String[] args) {
        Undertow server = Undertow.builder()
                .addHttpListener(8080, "localhost")
                .setHandler(
                        Handlers.path()
                                .addPrefixPath("/login", new LoginHandler(nameUsers, tokenUsers, messages))
                                .addPrefixPath("/logout", new LogoutHandler(nameUsers, tokenUsers, messages))
                                .addPrefixPath("/users", new UsersHandler(nameUsers, tokenUsers))
                                .addPrefixPath("/messages", new MessagesHandler(tokenUsers, messages))
                ).build();
        server.start();
        timeoutManager.start();
    }
}
