import com.google.gson.Gson;
import io.undertow.websockets.core.WebSockets;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class TimeoutManager extends Thread {
    private Map<UUID, User> tokenUsers;
    private Map<String, User> nameUsers;
    private List<Message> messages;
    private final int SERVER_ID = -1;
    private final int BYE_MESSAGE = -3;
    private final int KICKING = -4;

    public TimeoutManager(final Map nameUsers, final Map tokenUsers, List messages) {
        this.tokenUsers = tokenUsers;
        this.nameUsers = nameUsers;
        this.messages = messages;
    }

    private void kicking(final UUID userToken) {
        nameUsers.get(tokenUsers.get(userToken).getName()).toOffline();
        messages.add(new Message(KICKING, /* The notification about KICKING */
                (new Gson()).toJson(tokenUsers.get(userToken)), SERVER_ID));
        tokenUsers.entrySet().removeIf(entry -> entry.getKey().equals(userToken));
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(5000);
                for (UUID token: tokenUsers.keySet()) {
                    tokenUsers.get(token).increaseIdleTime();
                    if (tokenUsers.get(token).getIdleTime() > 30) {
                        kicking(token);
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
