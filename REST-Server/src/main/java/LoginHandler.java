import com.google.gson.Gson;
import io.undertow.io.IoCallback;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

import java.util.List;
import java.util.Map;
import java.util.UUID;

class  LoginHandler implements HttpHandler {
    private static final int SERVER_ID = -1;
    private static final int LOGINNING = -1;
    private Map<String, User> nameUsers;
    private Map<UUID, User> tokenUsers;
    private List<Message> messages;
    private static int idUser = 0;

    public LoginHandler(Map nameUsers, Map tokenUsers, List messages) {
        super();
        this.nameUsers = nameUsers;
        this.tokenUsers = tokenUsers;
        this.messages = messages;
    }

    private void userCreationProcedure(final String userName,
                                       final HttpServerExchange exchange) {
        UUID userToken = UUID.randomUUID();
        User newUser = new User(idUser++, userName, true, userToken);

        tokenUsers.put(userToken, newUser);
        nameUsers.put(userName, newUser);
        messages.add(new Message(LOGINNING, /* The notification about LOGIN for another users */
                (new Gson()).toJson(tokenUsers.get(userToken)), SERVER_ID));
        exchange.getResponseSender().send( /* Send an instance of the user */
                (new Gson()).toJson(newUser), IoCallback.END_EXCHANGE);
    }

    private void userOnlineProcedure(final HttpServerExchange exchange) {
        exchange.setStatusCode(401);
        exchange.getResponseHeaders().put(Headers.WWW_AUTHENTICATE,
                "Token realm='Username is already in use'");
    }

    private void userOfflineProcedure(final String userName,
                                      final HttpServerExchange exchange) {
        UUID userToken = UUID.randomUUID();

        nameUsers.get(userName).toOnline();
        nameUsers.get(userName).toZeroIdleTime();
        nameUsers.get(userName).setToken(userToken);
        tokenUsers.put(userToken, nameUsers.get(userName));

        messages.add(new Message(LOGINNING, /* The notification about LOGIN for another users */
                (new Gson()).toJson(tokenUsers.get(userToken)), SERVER_ID));
        exchange.getResponseSender().send( /* Send an instance of the user */
                (new Gson()).toJson(nameUsers.get(userName)), IoCallback.END_EXCHANGE);
    }

    private class UserInfo {
        public String username;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) {
        exchange.getRequestReceiver().receiveFullString((HttpServerExchange excng, String bytes) -> {
            UserInfo userInfo = (new Gson()).fromJson(bytes, UserInfo.class);

            if (!nameUsers.containsKey(userInfo.username)) {
                userCreationProcedure(userInfo.username, exchange);
            } else if (nameUsers.get(userInfo.username).isOnline()) {
                userOnlineProcedure(exchange);
            } else {
                userOfflineProcedure(userInfo.username, exchange);
            }
        });
    }
}