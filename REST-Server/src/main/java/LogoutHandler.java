import com.google.gson.Gson;
import io.undertow.io.IoCallback;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public class LogoutHandler implements HttpHandler {
    private Map<String, User> nameUsers;
    private Map<UUID, User> tokenUsers;
    private List<Message> messages;
    private static final int SERVER_ID = -1;
    private static final int LOGOUTING = -2;
    private final int BYE_MESSAGE = -3;

    public LogoutHandler(Map nameUsers, Map tokenUsers, List messages) {
        super();
        this.nameUsers = nameUsers;
        this.tokenUsers = tokenUsers;
        this.messages = messages;
    }

    private UUID getUserToken(HttpServerExchange exchange) throws IllegalAccessException {
        String strUserToken = exchange.getRequestHeaders().get("Authorization").get(0);

        if (strUserToken.isEmpty()) throw new IllegalAccessException();

        return UUID.fromString(strUserToken);
    }

    private void treatUserInstance(final UUID userToken)
            throws IllegalArgumentException, NullPointerException {
        nameUsers.get(tokenUsers.get(userToken).getName()).toOffline();
        nameUsers.get(tokenUsers.get(userToken).getName()).setToken(null);

        messages.add(new Message(LOGOUTING, /* The notification about LOGOUT for another users */
                (new Gson()).toJson(tokenUsers.get(userToken)), SERVER_ID));
        tokenUsers.entrySet().removeIf(entry -> entry.getKey().equals(userToken));
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) {
        try {
            UUID userToken = getUserToken(exchange);

            treatUserInstance(userToken);
            exchange.getResponseSender().send((new Gson()).toJson(new Message(BYE_MESSAGE,
                    "bye!", SERVER_ID)), IoCallback.END_EXCHANGE);
        } catch (IllegalArgumentException | NullPointerException uncorrTokExc) {
            exchange.setStatusCode(403);
        } catch (IllegalAccessException emptyTokenException) {
            exchange.setStatusCode(401);
        }
    }
}
