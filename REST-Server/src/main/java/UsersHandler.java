import com.google.gson.Gson;
import io.undertow.io.IoCallback;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class UsersHandler implements HttpHandler{
    private Map<String, User> nameUsers;
    private Map<UUID, User> tokenUsers;

    public UsersHandler(final Map nameUsers, final Map tokenUsers) {
        super();
        this.nameUsers = nameUsers;
        this.tokenUsers = tokenUsers;
    }

    private User getById(final int id) {
        for (User user: nameUsers.values()) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    private UUID getUserToken(HttpServerExchange exchange) throws IllegalAccessException {
        String strUserToken = exchange.getRequestHeaders().get("Authorization").get(0);

        if (strUserToken.isEmpty()) throw new IllegalAccessException();

        return UUID.fromString(strUserToken);
    }

    private void checkAuthorization(HttpServerExchange exchange)
            throws IllegalAccessException, IllegalArgumentException {
        UUID userToken = getUserToken(exchange);

        tokenUsers.get(userToken).getId();
    }

    class UsersDto {
        public List<User> users;

        public UsersDto(List<User> users) {
            this.users = users;
        }
    }

    private void sendUserInfo(HttpServerExchange exchange) throws NoSuchFieldError {
        int id = Integer.parseInt(exchange.getRelativePath().substring(1)); /* Getting a user's number */

        if (getById(id) == null) {
            throw new NoSuchFieldError();
        } else {
            exchange.getResponseSender().send((new Gson()).toJson(getById(id)), IoCallback.END_EXCHANGE);
        }
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) {
        try {
            checkAuthorization(exchange);
            if (exchange.getRelativePath().isEmpty()) {
                exchange.getResponseSender().send((new Gson()). /* Send a list with online users */
                        toJson(new UsersDto(new ArrayList<>(nameUsers.values()))), IoCallback.END_EXCHANGE);
            } else {
                sendUserInfo(exchange);
            }
        } catch (IllegalArgumentException | NullPointerException uncorrTokExc) {
            exchange.setStatusCode(403);
        } catch (IllegalAccessException emptyTokenException) {
            exchange.setStatusCode(401);
        } catch (NoSuchFieldError unknownUser) {
            exchange.setStatusCode(404);
        }
    }
}
