import java.io.IOException;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

public class MulticastReceiver extends Thread {
    private MulticastGroup MCGROUP;
    private final int STORAGE_LIFE = 5000;
    private final int MESSAGE_SIZE = 256;

    public MulticastReceiver(final MulticastGroup group){
        MCGROUP = group;
    }

    private void checkLastReceiving(Map<String,Long> idMap){
        idMap.entrySet().removeIf(entry -> System.currentTimeMillis() - entry.getValue() > STORAGE_LIFE);
    }

    private void receivePacket(Map<String, Long> idMap, final byte[] buf) {
        try {
            String received;
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
            MCGROUP.getSocket().receive(packet);
            received = new String(packet.getData(), 0, packet.getLength());
            idMap.put(received + packet.getAddress(), System.currentTimeMillis());
        } catch (IOException e) {
        }
    }

    public void run() {
        Map<String, Long> idMap = new HashMap<>();
        byte[] buf = new byte[MESSAGE_SIZE];
        String received = null;
        try {
            while (System.in.available() == 0) {
                receivePacket(idMap, buf);
                checkLastReceiving(idMap);
                System.out.println("Total instances: " + idMap.size());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            MCGROUP.getSocket().close();
        }
    }
}
