import java.io.IOException;
import java.net.DatagramPacket;

public class MulticastSender extends Thread {
    private MulticastGroup MCGROUP;
    private static final int SEND_PERIOD = 1000;

    public MulticastSender(final MulticastGroup group) {
        MCGROUP = group;
    }

    private void sendPacket(byte[] buf) throws InterruptedException, IOException{
        DatagramPacket packet
                = new DatagramPacket(buf, buf.length, MCGROUP.getAddress(), MCGROUP.getPort());
        MCGROUP.getSocket().send(packet);
        Thread.sleep(SEND_PERIOD);
    }

    public void run(){
        String multicastMessage = Long.toString(ProcessHandle.current().pid());
        byte[] buf = multicastMessage.getBytes();
        try {
            while(System.in.available() == 0) {
                sendPacket(buf);
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }
}
