import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

public class MulticastGroup {
    private MulticastSocket socket = null;
    private int multicastGroupPort;
    private InetAddress multicastGroupAddress;
    private final int TIME_EXPECTATION = 5000;

    public MulticastGroup(final String[] args) {
        try {
            multicastGroupAddress = InetAddress.getByName(args[0]);
            multicastGroupPort = Integer.parseInt(args[1]);
            socket = new MulticastSocket(multicastGroupPort);
            socket.setSoTimeout(TIME_EXPECTATION);
            socket.joinGroup(multicastGroupAddress);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public MulticastSocket getSocket(){
        return socket;
    }

    public InetAddress getAddress(){
        return multicastGroupAddress;
    }

    public int getPort(){
        return multicastGroupPort;
    }
}
