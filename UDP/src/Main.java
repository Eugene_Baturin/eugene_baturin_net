
public class Main {
    public static void main(String[] args) {
        if(args.length != 0) {
            App multicastApp = new App(args);
            multicastApp.start();
        } else {
            System.out.println("The command line is empty");
        }
    }
}
