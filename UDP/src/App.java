import java.net.InetAddress;
import java.net.UnknownHostException;

public class App {
    private MulticastGroup group;
    private MulticastReceiver receiver;
    private MulticastSender sender;

    public App(final String[] args) {
        checkArgs(args);
        group = new MulticastGroup(args);
        receiver = new MulticastReceiver(group);
        sender = new MulticastSender(group);
    }

    private void checkArgs(final String[] args) {
        try {
            Integer.parseInt(args[1]);
            if(!InetAddress.getByName(args[0]).isMulticastAddress()) {
                throw new UnknownHostException();
            }
        } catch (UnknownHostException e) {
            System.out.println("The typed parameters are uncorrect");
            System.exit(1);
        }
    }

    public void start(){
        sender.start();
        receiver.start();
    }
}
