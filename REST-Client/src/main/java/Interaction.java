import com.google.gson.Gson;
import okhttp3.*;

import java.io.IOException;
import java.util.Map;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

public class Interaction {
    private static Map<Integer, UserInfo> nameUsers;
    private boolean isLoggedIn = false;
    private Factory WSFactory;
    private OffsetCount offsetCount;
    private Timer messageGetterTimer;
    private OkHttpClient client;
    private final String baseUrl = "http://localhost:8080";

    public Interaction() {
        this.messageGetterTimer = new Timer();
        this.WSFactory = Factory.getInstance();
        this.client = new OkHttpClient();
        this.offsetCount = new OffsetCount(100, 0);
    }

    static {
        nameUsers = new ConcurrentHashMap<>();
    }

    private void callHandler(final String handlerName, OffsetCount offsetCount) {
        RequestHandler cmd = WSFactory.get(handlerName);

        if (cmd != null) {
            cmd.handle(isLoggedIn, nameUsers, client, baseUrl, offsetCount);
        }
    }

    private boolean loginProcedure() {
        int prevOffset;

        callHandler("LoginHandler", null);
        callHandler("UsersHandler", null);
        do {
            prevOffset = offsetCount.getOffset();
            callHandler("MessagesHandler", offsetCount);
        } while (offsetCount.getCount() == offsetCount.getOffset() - prevOffset);

        return true;
    }

    private void turnOnListener() {
        offsetCount.setCount(10);
        messageGetterTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                callHandler("MessagesHandler", offsetCount);
            }
        }, 0, 1000);
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        isLoggedIn = loginProcedure();

        turnOnListener();

        callHandler("InputHandler", null);
    }
}
