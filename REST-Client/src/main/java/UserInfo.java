import java.io.Serializable;
import java.util.UUID;

public class UserInfo implements Serializable {
    private final int id;
    private final String name;
    private boolean online;
    private UUID token;

    public UserInfo(final int id, final String name, final boolean online, final UUID token) {
        this.id = id;
        this.name = name;
        this.online = online;
        this.token = token;
    }

    public void setToken(final UUID token) {
        this.token = token;
    }

    public void toOnline() {
        this.online = true;
    }

    public void toOffline() {
        this.online = false;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public UUID getToken() {
        return token;
    }

    public boolean isOnline() {
        return online;
    }
}
