

public class OffsetCount {
    private int count;
    private int offset;

    public OffsetCount(final int count, final int offset) {
        this.count = count;
        this.offset = offset;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getCount() {
        return count;
    }

    public int getOffset() {
        return offset;
    }
}
