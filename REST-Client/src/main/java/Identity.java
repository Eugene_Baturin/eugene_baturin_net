


public enum Identity {
    INSTANCE;
    UserInfo userInfo;

    public UserInfo getInstance() {
        return userInfo;
    }

    public void setInstance(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
}