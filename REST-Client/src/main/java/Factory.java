public class Factory {
    private static Factory instance;

    private Factory() { }

    public static Factory getInstance() {
        try {
            if (instance == null) {
                instance = new Factory();
            }
            return instance;
        } catch (Exception ex) {
            System.out.println("ERROR: the factory can't be created " + ex);
            return null;
        }
    }

    RequestHandler get(String nameHandler){
        try {
            Class c = Class.forName(nameHandler);
            Object o = c.newInstance();
            return (RequestHandler) o;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
