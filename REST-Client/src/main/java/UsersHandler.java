import com.google.gson.Gson;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.http2.ErrorCode;

import javax.naming.AuthenticationException;
import java.io.IOException;
import java.rmi.UnknownHostException;
import java.util.List;
import java.util.Map;

public class UsersHandler implements RequestHandler {
    private static int HTTP_401 = 401;
    private static int HTTP_403 = 403;
    //private static int HTTP_404 = 404;

    public UsersHandler() {
        super();
    }

    class UsersDto {
        public List<UserInfo> users;

        public UsersDto(List<UserInfo> users) {
            this.users = users;
        }
    }

    private void initUsers(Response response,
                                Map<Integer, UserInfo> nameUsers) throws IOException {
        UsersDto usersList = (new Gson()).fromJson(response.body().string(), UsersDto.class);

        for (UserInfo user: usersList.users) {
            nameUsers.put(user.getId(), user);
        }
    }

    private void checkResponse(Response response,
                                  Map<Integer, UserInfo> nameUsers) throws IOException {
        try {
            if (response.code() == HTTP_401) {
                throw new UnknownHostException("Your token is empty");
            } else if (response.code() == HTTP_403) {
                throw new UnknownHostException("Your token is unknown");
            }
            if (response.body() != null) {
                initUsers(response, nameUsers);
            }
        } catch (UnknownHostException unHostEx) {
            System.out.println(unHostEx.getLocalizedMessage());
        } finally {
            if (response != null) response.close();
        }
    }

    public Response usersRequest(final OkHttpClient client,
                                 final String baseUrl) throws IOException {
        Request request = new Request.Builder()
                .url(baseUrl + "/users")
                .header("Authorization", Identity.INSTANCE.getInstance().getToken().toString())
                .method("GET", null)
                .build();

        return client.newCall(request).execute();
    }

    @Override
    public void handle(boolean isLoggedIn, Map<Integer, UserInfo> nameUsers,
                          final OkHttpClient client, final String baseUrl, OffsetCount offsetCount) {
        try {
            Response response = usersRequest(client, baseUrl);

            checkResponse(response, nameUsers);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
