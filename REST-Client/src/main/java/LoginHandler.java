import com.google.gson.Gson;
import okhttp3.*;

import javax.naming.AuthenticationException;
import java.io.IOException;
import java.util.Map;
import java.util.Scanner;

public class LoginHandler implements RequestHandler {
    private static int HTTP_401 = 401;
    private static final int SERVER_ID = -1;

    public LoginHandler() {
        super();
    }

    private void identification(Response response,
                                Map<Integer, UserInfo> nameUsers) throws IOException {
        UserInfo userInfo = (new Gson()).fromJson(response.body().string(), UserInfo.class);
        Identity identity = Identity.INSTANCE;

        identity.setInstance(userInfo);
        nameUsers.put(userInfo.getId(), userInfo);

        System.out.println("Log-in successful!");
    }

    private boolean checkResponse(Response response,
                                  Map<Integer, UserInfo> nameUsers) throws IOException {
        try {
            if (response.code() == HTTP_401) {
                throw new AuthenticationException();
            }
            if (response.body() != null) {
                identification(response, nameUsers);
                return true;
            }
        } catch (AuthenticationException authEx) {
            System.out.println(response.headers().get("WWW-Authenticate"));
        } finally {
            if (response != null) response.close();
        }
        return false;
    }

    private class UserName {
        public String username;

        public UserName(final String username) {
            this.username = username;
        }
    }

    private Response loginRequest(final String userName, final OkHttpClient client,
                                  final String baseUrl) throws IOException {
        String body = (new Gson()).toJson(new UserName(userName));
        RequestBody requestBody = RequestBody.create(MediaType.get("application/json"), body);
        Request request = new Request.Builder()
                .url(baseUrl + "/login")
                .method("POST", requestBody)
                .build();

        return client.newCall(request).execute();
    }

    @Override
    public void handle(boolean isLoggedIn, Map<Integer, UserInfo> nameUsers,
                       final OkHttpClient client, final String baseUrl, OffsetCount offsetCount) {
        Scanner scanner = new Scanner(System.in);
        Response response;
        try {
            while (!isLoggedIn){
                System.out.print("Login as: ");
                response = loginRequest(scanner.nextLine(), client, baseUrl);
                isLoggedIn = checkResponse(response, nameUsers);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
