import com.google.gson.Gson;
import okhttp3.*;

import java.io.IOException;
import java.util.Map;

public interface RequestHandler {

    public void handle(boolean isLoggedIn, Map<Integer, UserInfo> nameUsers,
                       OkHttpClient client, String baseUrl, OffsetCount offsetCount);

}