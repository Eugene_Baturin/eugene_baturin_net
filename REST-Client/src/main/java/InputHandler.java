import com.google.gson.Gson;
import okhttp3.*;

import java.io.IOException;
import java.util.Map;
import java.util.Scanner;

public class InputHandler implements RequestHandler {
    private boolean isExitRequest = false;

    public InputHandler() {
        super();
    }

    class MessageContent {
        public String message;

        public MessageContent(final String message) {
            this.message = message;
        }
    }

    private Response sendLogout(final OkHttpClient client,
                               final String baseUrl) throws IOException {
        RequestBody body = RequestBody.create(MediaType.get("application/json"), "");
        Request request = new Request.Builder()
                .url(baseUrl + "/logout")
                .header("Authorization", Identity.INSTANCE.getInstance().getToken().toString())
                .method("POST", body)
                .build();
        return client.newCall(request).execute();
    }

    private Response sendMessage(final String message, final OkHttpClient client,
                                final String baseUrl) throws IOException {
        RequestBody body = RequestBody.create(
                MediaType.get("application/json"), (new Gson()).toJson(new MessageContent(message)));
        Request request = new Request.Builder()
                .url(baseUrl + "/messages")
                .header("Authorization", Identity.INSTANCE.getInstance().getToken().toString())
                .method("POST", body)
                .build();
        return client.newCall(request).execute();
    }

    private void turnOnSender(Scanner scanner,  OkHttpClient client, final String baseUrl) {
        String message = new String(scanner.nextLine());
        Response response = null;

        try {
            if (message.equals("/logout")) {
                response = sendLogout(client, baseUrl);
                MessageContent content = (new Gson()).
                        fromJson(response.body().string(), MessageContent.class);
                System.out.println("Server: " + content.message);
                System.exit(0);
            }
            response = sendMessage(message, client, baseUrl);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (response != null) response.close();
        }
    }

    @Override
    public void handle(boolean isLongPolling, Map<Integer, UserInfo> nameUsers,
                       OkHttpClient client, final String baseUrl, OffsetCount offsetCount) {
        Scanner scanner = new Scanner(System.in);

        while (!isExitRequest) {
            turnOnSender(scanner, client, baseUrl);
        }
    }

}
