import com.google.gson.Gson;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.rmi.UnknownHostException;
import java.util.List;
import java.util.Map;

public class MessagesHandler implements RequestHandler {
    private static final int LOGINNING = -1;
    private static final int LOGOUTING = -2;
    private static final int BYE_MESSAGE = -3;
    private static final int KICKING = -4;
    private static int HTTP_401 = 401;
    private static int HTTP_403 = 403;
    private static int numMessage = 0;

    public MessagesHandler() {
        super();
    }

    class MessagesDto {
        public List<Message> messages;

        public MessagesDto(List<Message> messages) {
            this.messages = messages;
        }
    }

    private void connecting(final Message messObj, Map<Integer, UserInfo> nameUsers,
                            OffsetCount offsetCount, boolean isLongPolling) {
        UserInfo newUser = (new Gson()).fromJson(messObj.getMessage(), UserInfo.class);

        if (isLongPolling) {
            if (nameUsers.containsKey(newUser.getId())) {
                nameUsers.get(newUser.getId()).toOnline();
            } else {
                nameUsers.put(newUser.getId(), newUser);
            }
        }
        System.out.println("Notification: " + newUser.getName() + " has been connected");
        offsetCount.setOffset(++numMessage);
    }

    private void detaching(final Message messObj, Map<Integer, UserInfo> nameUsers,
                           OffsetCount offsetCount, boolean isLongPolling) {
        UserInfo detachedUser = (new Gson()).
                fromJson(messObj.getMessage(), UserInfo.class);

        if (isLongPolling) {
            nameUsers.get(detachedUser.getId()).toOffline();
        }
        System.out.println("Notification: " + detachedUser.getName() + " has been detached");
        offsetCount.setOffset(++numMessage);
    }

    private void kicking(final Message messObj, Map<Integer, UserInfo> nameUsers,
                         OffsetCount offsetCount, boolean isLongPolling) {
        UserInfo kickedUser = (new Gson()).
                fromJson(messObj.getMessage(), UserInfo.class);

        if (isLongPolling) {
            nameUsers.get(kickedUser.getId()).toOffline();
        }

        System.out.println("Notification: " + kickedUser.getName() + " has been kicked for idling");
        offsetCount.setOffset(++numMessage);

        if (isLongPolling && Identity.INSTANCE.getInstance().getName().equals(kickedUser.getName())) {
            System.exit(0);
        }
    }

    private void serverMessagesFork(final Message messObj, final Map<Integer, UserInfo> nameUsers,
                                    OffsetCount offsetCount, boolean isLongPolling) {
        if (messObj.getId() == LOGINNING) {
            connecting(messObj, nameUsers, offsetCount, isLongPolling);
        } else if (messObj.getId() == LOGOUTING) {
            detaching(messObj, nameUsers, offsetCount, isLongPolling);
        } else if (messObj.getId() == KICKING) {
            kicking(messObj, nameUsers, offsetCount, isLongPolling);
        } else if (messObj.getId() == BYE_MESSAGE) {
            System.out.println("Server: " + messObj.getMessage());
            System.exit(0);
        } else {
            System.out.println(nameUsers.get(messObj.getAuthor()).getName() + ": " + messObj.getMessage());
            offsetCount.setOffset(++numMessage);
        }
    }

    private void readMessages(Response response, Map<Integer, UserInfo> nameUsers,
                              OffsetCount offsetCount, boolean isLongPolling) throws IOException {
        MessagesDto messList = (new Gson()).
                fromJson(response.body().string(), MessagesDto.class);

        for (Message message: messList.messages) {
            serverMessagesFork(message, nameUsers, offsetCount, isLongPolling);
        }
    }

    private void checkResponse(boolean isLongPolling, Response response, Map<Integer, UserInfo> nameUsers,
                               OffsetCount offsetCount) throws IOException, UnknownHostException {
        if (response.code() == HTTP_401) {
            throw new UnknownHostException("Your token is empty");
        } else if (response.code() == HTTP_403) {
            throw new UnknownHostException("Your token is unknown");
        } else if (response.body() != null) {
            readMessages(response, nameUsers, offsetCount, isLongPolling);
        }
    }

    public Response messagesRequest(final OkHttpClient client, final String baseUrl,
                                    OffsetCount offsetCount) throws IOException {
        Request request = new Request.Builder()
                .url(baseUrl + "/messages?offset=" + offsetCount.getOffset()
                        + "&count=" + offsetCount.getCount())
                .header("Authorization", Identity.INSTANCE.getInstance().getToken().toString())
                .method("GET", null)
                .build();

        return client.newCall(request).execute();
    }

    @Override
    public void handle(boolean isLongPolling, Map<Integer, UserInfo> nameUsers,
                       OkHttpClient client, String baseUrl, OffsetCount offsetCount) {
        Response response = null;

        try {
            response = messagesRequest(client, baseUrl, offsetCount);

            checkResponse(isLongPolling, response, nameUsers, offsetCount);
        } catch (UnknownHostException unHostEx) {
            System.out.println(unHostEx.getLocalizedMessage());//????
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (response != null) response.close();
        }
    }
}
