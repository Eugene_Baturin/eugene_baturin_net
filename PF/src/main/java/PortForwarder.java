import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.Set;

public class PortForwarder {
    private InetSocketAddress serverAddress;
    private InetAddress rhost;
    private Selector selector;
    private int lport, rport;

    public PortForwarder(String args[]) throws IOException {
        lport = Integer.valueOf(args[0]);
        rhost = InetAddress.getByName(args[1]);
        rport = Integer.valueOf(args[2]);
        serverAddress = new InetSocketAddress(rhost, rport);
    }

    private void accept(SelectionKey key) {
        SocketChannel clientChannel, serverChannel;
        try {
            clientChannel = ((ServerSocketChannel)key.channel()).accept();
            clientChannel.configureBlocking(false);
            serverChannel = SocketChannel.open();
            serverChannel.configureBlocking(false);
            serverChannel.connect(serverAddress);

            Attachment client = new Attachment(clientChannel, selector);
            Attachment server = new Attachment(serverChannel, selector);
            client.setOtherAttachment(server);
            server.setOtherAttachment(client);
            serverChannel.register(selector, SelectionKey.OP_CONNECT, server);
            clientChannel.register(selector,0, client);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void read(SelectionKey key) {
        Attachment attachment = (Attachment)key.attachment();
        try {
            int byteRead = attachment.getSocketChannel().read(attachment.getBuf());
            if (byteRead > 0) {
                attachment.getOtherAttachment().addOption(SelectionKey.OP_WRITE);
            } else if (byteRead == -1) {
                attachment.deleteOption(SelectionKey.OP_READ);
            }
        } catch (IOException e) {
            e.printStackTrace();
            attachment.close();
        }
    }

    private void write(SelectionKey key) {
        Attachment attachment = (Attachment) key.attachment();
        attachment.getOtherAttachment().getBuf().flip();
        try {
            int byteWrite = attachment.getSocketChannel().write(attachment.getOtherAttachment().getBuf());
            if (byteWrite > 0) {
                attachment.getOtherAttachment().getBuf().compact();
                attachment.getOtherAttachment().addOption(SelectionKey.OP_READ);
            }
            if (attachment.getOtherAttachment().getBuf().position() == 0) {
                attachment.deleteOption(SelectionKey.OP_WRITE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void connect(SelectionKey key) {
        SocketChannel channel = ((SocketChannel) key.channel());
        Attachment attachment = ((Attachment) key.attachment());
        try {
            channel.finishConnect();
            attachment.deleteOption(SelectionKey.OP_CONNECT);
            attachment.addOption(SelectionKey.OP_READ);
            attachment.getOtherAttachment().addOption(SelectionKey.OP_READ);
        } catch (IOException e) {
            e.printStackTrace();
            attachment.close();
        }
    }

    private void selectorInit() throws IOException {
        selector = Selector.open();
        ServerSocketChannel serverSocket;
        serverSocket = ServerSocketChannel.open();
        serverSocket.bind(new InetSocketAddress(lport));
        serverSocket.configureBlocking(false);
        serverSocket.register(selector, SelectionKey.OP_ACCEPT);
    }

    public void run() throws IOException {
        selectorInit();
        while (true) {
            selector.select();
            Set<SelectionKey> selectedKeys = selector.selectedKeys();
            Iterator<SelectionKey> iter = selectedKeys.iterator();
            while (iter.hasNext()) {
                SelectionKey key = iter.next();
                if (key.isValid() && key.isAcceptable()) {
                    System.out.println("Accept");
                    accept(key);
                }
                if (key.isValid() && key.isConnectable()) {
                    System.out.println("Connect");
                    connect(key);
                }
                if (key.isValid() && key.isReadable()) {
                    System.out.println("Read");
                    read(key);
                }
                if (key.isValid() && key.isWritable()) {
                    System.out.println("Write");
                    write(key);
                }
                iter.remove();
            }
        }
    }
}
