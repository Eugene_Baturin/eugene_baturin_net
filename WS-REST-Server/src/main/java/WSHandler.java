import io.undertow.websockets.core.WebSocketChannel;
import java.util.List;
import java.util.Map;

public interface WSHandler {

    public boolean handle(WebSocketChannel channel, final String data, final String userName,
                       Map<String, User> nameUsers, List messages);

}