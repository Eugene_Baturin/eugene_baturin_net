import io.undertow.Undertow;
import io.undertow.websockets.core.WebSocketChannel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static io.undertow.Handlers.path;
import static io.undertow.Handlers.websocket;

public class Main {
    private static Map nameUsers;
    private static List messages;

    static {
        nameUsers = new ConcurrentHashMap<String, User>();
        messages = new ArrayList<Message>();
    }

    public static void main(final String[] args) {
        Undertow server = Undertow.builder()
                .addHttpListener(8080, "localhost")
                .setHandler(
                        path().addPath("/chat",
                                websocket(new WebSocketHandler(nameUsers, messages)))
                ).build();
        server.start();
    }
}
