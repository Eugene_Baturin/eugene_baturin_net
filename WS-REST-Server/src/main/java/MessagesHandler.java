import com.google.gson.Gson;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;

import java.util.List;
import java.util.Map;

public class MessagesHandler implements WSHandler {
    private static int idMessage = 0;

    public MessagesHandler() {
        super();
    }

    private void messageSaving(final User user, final String message,
                               List messages, final Map<String, User> nameUsers) {
        Message newMessage = new Message(idMessage++, message, user.getId());

        messages.add(newMessage);

        for (String name: nameUsers.keySet()) {
            WebSockets.sendText((new Gson()). /* Send information about the new connection */
                    toJson(newMessage), nameUsers.get(name).getChannel(), null);
        }
    }

    class MessageContent {
        public String message;
    }

    @Override
    public boolean handle(WebSocketChannel channel, final String message,
                       final String userName, Map<String, User> nameUsers, List messages){
        MessageContent content = (new Gson()).fromJson(message, MessageContent.class);

        messageSaving(nameUsers.get(userName), content.message, messages, nameUsers);

        return true;
    }
}
