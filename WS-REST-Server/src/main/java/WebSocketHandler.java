import com.google.gson.Gson;
import io.undertow.websockets.WebSocketConnectionCallback;
import io.undertow.websockets.core.AbstractReceiveListener;
import io.undertow.websockets.core.BufferedTextMessage;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.spi.WebSocketHttpExchange;

import java.util.List;
import java.util.Map;

public class WebSocketHandler implements WebSocketConnectionCallback {
    private Map<String, User> nameUsers;
    private List<Message> messages;
    private Factory WSFactory;

    public WebSocketHandler(Map nameUsers, List messages) {
        super();
        this.nameUsers = nameUsers;
        this.messages = messages;
        this.WSFactory = Factory.getInstance();
    }

    private boolean callHandler(WebSocketChannel channel, Map nameUsers, List messages,
                             final String message, final String handlerName, final String userName) {
        WSHandler cmd = WSFactory.get(handlerName);

        if (cmd != null) {
            return cmd.handle(channel, message, userName, nameUsers, messages);
        }

        return false;
    }

    public class UserInfo {
        public String username;
    }

    public class BooleanHolder {
        public Boolean bool;

        public BooleanHolder(Boolean bool) {
            this.bool = bool;
        }
    }

    @Override
    public void onConnect(WebSocketHttpExchange exchange, WebSocketChannel channel) {
        channel.getReceiveSetter().set(new AbstractReceiveListener() {
            boolean isLoggedIn = false;
            UserInfo userInfo;
            String data;
            @Override
            protected void onFullTextMessage(WebSocketChannel channel, BufferedTextMessage message) {
                data = message.getData();
                if (!isLoggedIn) {
                    userInfo = (new Gson()).fromJson(data, UserInfo.class);
                    isLoggedIn = callHandler(channel, nameUsers, messages, null,
                            "LoginHandler", userInfo.username);
                } else if (data.equals("{\"message\":\"/logout\"}")) {
                    callHandler(channel, nameUsers, messages, data,
                            "LogoutHandler", userInfo.username);
                } else {
                    callHandler(channel, nameUsers, messages, data,
                            "MessagesHandler", userInfo.username);
                }
            }
        });
        channel.resumeReceives();
    }
}

