import io.undertow.websockets.core.WebSocketChannel;

import java.io.Serializable;
import java.util.UUID;

public class User implements Serializable {
    private final int id;
    private final String name;
    private boolean online;
    private transient WebSocketChannel channel;

    public User(final int id, final String name, final boolean online, final WebSocketChannel channel) {
        this.id = id;
        this.name = name;
        this.online = online;
        this.channel = channel;
    }

    public void toOnline() {
        this.online = true;
    }

    public void toOffline() {
        this.online = false;
    }

    public int getId() {
        return id;
    }

    public WebSocketChannel getChannel() { return channel; }

    public String getName() {
        return name;
    }

    public boolean isOnline() {
        return online;
    }
}
