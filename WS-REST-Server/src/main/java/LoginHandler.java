import com.google.gson.Gson;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LoginHandler implements WSHandler {
    private static int idUser = 0;
    private final int SERVER_ID = -1;
    private final int LOGINNING = -1;
    private final int ALREADY_IN_USE = -1;

    public LoginHandler () {
        super();
    }

    class UsersDto {
        public List<User> users;

        public UsersDto(List<User> users) {
            this.users = users;
        }
    }

    class MessagesDto {
        public List<Message> messages;

        public MessagesDto(List<Message> messages) {
            this.messages = messages;
        }
    }

    private void sendRequiredInformation(WebSocketChannel channel, final List messages,
                                         final User newUser, final Map<String, User> nameUsers) {
        WebSockets.sendText((new Gson()). /* Send information about the new user */
                toJson(newUser), channel, null);
        WebSockets.sendText((new Gson()). /* Send a list with online users */
                toJson(new UsersDto(new ArrayList(nameUsers.values()))), channel, null);
        WebSockets.sendText((new Gson()). /* Send a list with messages */
                toJson(new MessagesDto(messages)), channel, null);
    }

    private boolean userCreationProcedure(final String userName, WebSocketChannel channel,
                                       final Map<String, User> nameUsers, final List messages) {
        Message loginningNotification;
        User newUser = new User(idUser++, userName, true, channel);

        sendRequiredInformation(channel, messages, newUser, nameUsers);

        for (String name: nameUsers.keySet()) {
            loginningNotification = new Message(LOGINNING, (new Gson()).toJson(newUser), SERVER_ID);
            WebSockets.sendText((new Gson()). /* Send information about the new connection */
                    toJson(loginningNotification), nameUsers.get(name).getChannel(), null);
        }
        nameUsers.put(userName, newUser);

        return true;
    }

    private boolean userOnlineProcedure(WebSocketChannel channel) {
        User newUser = new User(ALREADY_IN_USE, null, false, null);

        WebSockets.sendText((new Gson()). /* Send "Username is already in use" */
                toJson(newUser), channel, null);

        return false;
    }

    private boolean userOfflineProcedure(final String userName,
                                      final WebSocketChannel channel, Map<String, User> nameUsers) {
        nameUsers.get(userName).toOnline();

        for (WebSocketChannel session : channel.getPeerConnections()) {
            WebSockets.sendText((new Gson()).toJson(nameUsers.get(userName)), session, null);
        }

        return true;
    }

    @Override
    public boolean handle(WebSocketChannel channel, String string,
                       final String userName, Map<String, User> nameUsers, List messages){
        if (!nameUsers.containsKey(userName)) {
            return userCreationProcedure(userName, channel, nameUsers, messages);
        } else if (nameUsers.get(userName).isOnline()) {
            return userOnlineProcedure(channel);
        } else {
            return userOfflineProcedure(userName, channel, nameUsers);
        }
    }
}