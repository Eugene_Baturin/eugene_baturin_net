import java.io.*;
import java.net.*;

public class Sender {
    private DatagramSocket clientSocket;

    public Sender() throws SocketException {
        clientSocket = new DatagramSocket();
    }

    public void sendPacket(final HostAddress receiverAddress, Packet packet){
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(outputStream);
            os.writeObject(packet);
            byte[] data = outputStream.toByteArray();
            DatagramPacket sendPacket = new DatagramPacket(data, data.length,
                    receiverAddress.getIp(), receiverAddress.getPort());
            clientSocket.send(sendPacket);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}