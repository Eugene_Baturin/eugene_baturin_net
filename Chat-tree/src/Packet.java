import java.io.Serializable;
import java.net.InetAddress;
import java.util.UUID;

public class Packet implements Serializable {
    private final String operation;
    private final UUID identificator;
    private final HostAddress senderAddress;
    private final String nodeName;
    private final Object data;

    /* Telescoping constructor pattern */
    public Packet(final String operation, final UUID identificator,
                final HostAddress senderAddress, final String nodeName){
        this(operation, identificator, senderAddress, nodeName, null);
    }
    public Packet(final String operation, final UUID identificator,
                   final HostAddress senderAddress, final String nodeName, final Object data){
        this.operation = operation;
        this.identificator = identificator;
        this.senderAddress = senderAddress;
        this.nodeName = nodeName;
        this.data = data;
    }

    public boolean equals(final Packet packet) {
        if (this.getOperation().equals(packet.getOperation()) &&
                this.getIdentificator().equals(packet.getIdentificator()) &&
                this.getHostAddress().equals(packet.getHostAddress()) &&
                this.getNodeName().equals(packet.getNodeName()) &&
                this.getData().equals(packet.getData())) {
            return true;
        } else {
            return false;
        }
    }

    public String toString() {
        return this.getOperation() + " " + this.getIdentificator().toString() + " " +
                this.getHostAddress().toString() + " " + this.getNodeName()+ " " + this.getData();
    }

    public String getOperation(){
        return this.operation;
    }

    public UUID getIdentificator() {
        return identificator;
    }

    public HostAddress getHostAddress() {
        return senderAddress;
    }

    public String getNodeName() {
        return nodeName;
    }

    public Object getData() {
        return data;
    }
}
