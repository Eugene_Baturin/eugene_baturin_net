import java.io.Serializable;
import java.net.InetAddress;

public class HostAddress implements Serializable {
    private final InetAddress ip;
    private final int port;

    public HostAddress(final InetAddress ip, final int port) {
        this.ip = ip;
        this.port = port;
    }

    public boolean equals(final HostAddress address) {
        if(this.getIp().equals(address.getIp()) && this.getPort() == address.getPort()) {
            return true;
        } else {
            return false;
        }
    }

    public String toString() {
        return this.getIp().toString() + ":" + this.getPort();
    }

    public InetAddress getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }
}
