import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.UUID;

public class Server {

    public Server() {

    }

    public void connectHandler(final HostAddress parentAddress,
                               final HostAddress nodeAddress, final String nodeName) {
        try {
            if(parentAddress.getIp() != null) {
                UUID idPacket = UUID.randomUUID();
                new Thread(new Interpretator(new Packet("CONNECTION",
                        idPacket, nodeAddress, nodeName), nodeAddress, parentAddress)).start();
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public void outwardHandler(final HostAddress nodeAddress, final String nodeName) {
        new Thread(() -> {
            try {
                BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
                while (true) {
                    String data = inFromUser.readLine();
                    UUID idPacket = UUID.randomUUID();
                    Packet packet = new Packet("MESSAGE", idPacket, nodeAddress, nodeName, data);
                    new Thread(new Interpretator(packet, nodeAddress)).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void incomingHandler(final HostAddress nodeAddress){
        new Thread(() -> {
            try{
                while (true) {
                    Receiver receiver = new Receiver(nodeAddress.getPort());
                    Packet packet = receiver.receivePacket();
                    //interpretator1.treat(packet, neighbours, nodeAddress);
                    new Thread(new Interpretator(packet, nodeAddress)).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
