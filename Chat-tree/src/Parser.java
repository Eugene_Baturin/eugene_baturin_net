import java.net.InetAddress;
import java.net.UnknownHostException;
import com.google.common.net.InetAddresses;

public class Parser {
    private String nodeName;
    private int nodePort;
    private int parentPort = 0;
    private InetAddress parentIp = null;
    private int lossPercentage;
    //private final Node node;

    public Parser(final String[] args) {
        parseAttributes(args);
        //this.node = new Node(nodeName, nodePort, lossPercentage, parentIp, parentPort);
    }

    private InetAddress checkIpAddress(final String ipString) throws UnknownHostException {
        InetAddress inetAddress = InetAddress.getByName(ipString);
        if (!InetAddresses.isInetAddress(ipString)){
            throw new UnknownHostException();
        }
        return inetAddress;
    }

    private int checkInt(final String str, final int lowerThreshold,
                          final int upperThreshold) throws UnknownHostException {
        int integer = Integer.parseInt(str);
        if (!(integer >= lowerThreshold && integer <= upperThreshold)){
            throw new UnknownHostException();
        }
        return integer;
    }

    private void parseOptionalParameters(final String[] args) throws UnknownHostException {
        if(args.length == 5){
            this.parentIp = checkIpAddress(args[3]); /* Parent's IP check */
            this.parentPort = checkInt(args[4], 4010, 4040); /* Parent's port check */
        }
    }

    private void parseAttributes(final String[] args) {
        try {
            this.nodeName = args[0];
            this.lossPercentage = checkInt(args[1], 0, 100); /* Loss petcentage check */
            this.nodePort = checkInt(args[2], 4010, 4040); /* Node's port check */
            parseOptionalParameters(args);
        } catch (UnknownHostException e) {
            System.out.println("The typed parameters are uncorrect");
            System.exit(1);
        }
    }

    public void start(){

    }
}