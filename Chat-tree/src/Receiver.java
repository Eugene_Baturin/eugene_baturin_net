import java.io.*;
import java.net.*;

public class Receiver {
    private DatagramSocket serverSocket;

    public Receiver(final int port) throws SocketException {
        serverSocket = new DatagramSocket(port);
    }

    public Packet receivePacket() {
        try {
            byte[] incomingData = new byte[1024];
            DatagramPacket incomingPacket = new DatagramPacket(incomingData, incomingData.length);
            serverSocket.receive(incomingPacket);
            byte[] data = incomingPacket.getData();
            ByteArrayInputStream in = new ByteArrayInputStream(data);
            ObjectInputStream is = new ObjectInputStream(in);
            Packet packet = (Packet)is.readObject();
            return packet;
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            serverSocket.close();
        }
        return null;
    }
}

