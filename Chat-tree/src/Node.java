import java.net.*;

public class Node {
    private final HostAddress nodeAddress;
    private final HostAddress parentAddress;
    private final String nodeName;
    private final int lossPercentage;
    private final Server server;

    /* Telescoping constructor pattern */
    public Node(final String nodeName, final int nodePort,
                final int lossPercentage) throws UnknownHostException {
        this(nodeName, nodePort, lossPercentage, null, 0);
    }
    public Node(final String nodeName, final int nodePort, final int lossPercentage,
                final InetAddress parentIp, final int parentPort) throws UnknownHostException {
        this.parentAddress = new HostAddress(parentIp, parentPort);
        this.nodeName = nodeName;
        this.lossPercentage = lossPercentage;
        this.server = new Server();
        this.nodeAddress = new HostAddress(
                InetAddress.getByName(InetAddress.getLocalHost().getHostAddress()), nodePort);
    }

    public void installNode(){
        server.connectHandler(parentAddress, nodeAddress, nodeName);
        server.outwardHandler(nodeAddress, nodeName);
        server.incomingHandler(nodeAddress);
    }
}
