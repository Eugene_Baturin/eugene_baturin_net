import java.net.SocketException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Interpretator extends Thread {
    private Sender sender;
    private Packet packet;
    private HostAddress nodeAddress;
    private HostAddress parentAddress;
    private static Set<HostAddress> neighbours;
    private static ConcurrentHashMap<UUID, Packet> messages;
    private static ConcurrentHashMap<UUID, Map<HostAddress, Integer>> sendingMap;
    private static ConcurrentHashMap<UUID, HostAddress> receivingMap;

    /* Telescoping constructor pattern */
    public Interpretator(Packet packet,
                         HostAddress nodeAddress) throws SocketException {
        this(packet, nodeAddress, null);
    }
    public Interpretator(Packet packet, HostAddress nodeAddress,
                         HostAddress parentAddress) throws SocketException {
        this.packet = packet;
        this.nodeAddress = nodeAddress;
        this.parentAddress = parentAddress;
        this.sender = new Sender();
    }

    static {
        sendingMap = new ConcurrentHashMap<>();
        receivingMap = new ConcurrentHashMap<>();
        messages = new ConcurrentHashMap<>();
        neighbours = new HashSet<>();
    }

    private void sendConfirmation(final HostAddress receiver) {
        if (sendingMap.get(packet.getIdentificator()).isEmpty() && receivingMap.containsKey(packet.getIdentificator())) {
            sender.sendPacket(receiver, new Packet( /* Send a confirmation about receiving */
                    "CONFIRMATION", packet.getIdentificator(),
                    nodeAddress, packet.getNodeName()));
        }
    }

    private void setSendingMap(final HostAddress prevHost) {
        Map<HostAddress, Integer> tableOfReceiving = new HashMap<>();

        for (HostAddress e: neighbours) {
            if(!e.equals(prevHost)) {
                tableOfReceiving.put(new HostAddress(e.getIp(), e.getPort()), 0);
            }
        }

        sendingMap.put(packet.getIdentificator(), tableOfReceiving);
    }

    private void setMaps(final HostAddress prevHost){
        setSendingMap(prevHost);

        if (!packet.getHostAddress().equals(nodeAddress)) {
            receivingMap.put(packet.getIdentificator(), packet.getHostAddress());
        } else {
            messages.put(packet.getIdentificator(), packet);
        }
    }

    private void messaging() {
        if (!receivingMap.contains(packet.getIdentificator())) {
            System.out.println(packet.getData());
            setMaps(packet.getHostAddress());
        }
        for (HostAddress e: sendingMap.get(packet.getIdentificator()).keySet()) {
            sender.sendPacket(e, new Packet("MESSAGE", packet.getIdentificator(),
                    nodeAddress, packet.getNodeName(), packet.getData()));
        }
        sendConfirmation(receivingMap.get(packet.getIdentificator()));
    }

    private void confirmation() {
        System.out.println("CONFIRMATION by " + packet.getHostAddress().getPort());

        sendingMap.get(packet.getIdentificator()).entrySet().
                removeIf(entry -> entry.getKey().equals(packet.getHostAddress()));

        sendConfirmation(receivingMap.get(packet.getIdentificator()));
    }

    private void connection() {
        sender.sendPacket(parentAddress, new Packet(
                "ADDRESSING", packet.getIdentificator(),
                nodeAddress, packet.getNodeName()));

        neighbours.add(parentAddress);
    }

    private void addressing() {
        System.out.println(packet.getNodeName() + " connected");
        neighbours.add(packet.getHostAddress());
    }

    public void run(){
        if(packet.getOperation().equals("MESSAGE")){
            messaging();
        }
        if(packet.getOperation().equals("CONFIRMATION")){
            confirmation();
        }
        if(packet.getOperation().equals("CONNECTION")){
            connection();
        }
        if(packet.getOperation().equals("ADDRESSING")){
            addressing();
        }
    }
}
