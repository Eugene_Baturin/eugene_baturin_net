package calc;

import java.io.*;
import java.util.*;

public class Parser{
    private BufferedReader br;

    public void opening(String[] args){
        try {
            Reader isr ;
            if(args.length == 0) isr = new InputStreamReader(System.in);
            else if(args.length == 1) isr = new InputStreamReader(new FileInputStream(args[0]));
            else throw new Exception("Input data are incorrect");
            br = new BufferedReader(isr);
        } catch ( Exception ex ) {
            System.out.println("ERROR in Parser(openingTxt):" + ex);
        }
    };

    public void writingToList(List curList){
        try {
            String curStr;
            while(((curStr = br.readLine()) != null)&&(curStr.length()!=0)){
                if(!(curStr.startsWith("#"))) {
                    String commandOrValue;
                    Scanner stringScanner = new Scanner(curStr);
                    List curString = new ArrayList<String>();
                    curList.add(curString);
                    while (stringScanner.hasNext()) {
                        commandOrValue = stringScanner.useDelimiter("\\s+").next();
                        curString.add(commandOrValue);
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("ERROR in ParserTxt(writingToList):" + ex);
        }
    };

    public void closing(){
        try {
            br.close();
        } catch ( Exception ex ) {
            System.out.println("ERROR in ParserTxt(closingTxt):" + ex);
        }
    };

    public void openWriteClose(String[] args, List curList){
        opening(args);
        writingToList(curList);
        closing();
    };
}
