package calc;

import java.util.List;

public interface Command {

    public void execute(List<String> curList, Context context);

}