package calc;

import java.util.ArrayList;

public class Calculator {
    private Factory calcFact;
    private Context calcCont;
    private Parser parser;
    private ArrayList<ArrayList<String>> calcList;

    Calculator(String configurationFile){
        try{
            calcFact = Factory.getInstance(configurationFile);
            calcCont = new Context();
            calcList = new ArrayList();
            parser = new Parser();
        } catch (Exception ex) {
            System.out.println("ERROR in calc.Calculator(constructor):" + ex);
        }
    };

    public void parsing(String[] args) {
        parser.openWriteClose(args, calcList);
    }

    public void inputConsole(String[] args){
        parsing(args);
        treatment();
    }

    public void treatment(){
        try{
            for (ArrayList<String> str : calcList) {
                Command cmd = calcFact.get(str.get(0));
                if(cmd!=null) {
                    cmd.execute(str, calcCont);
                }
            }
        } catch (Exception ex) {
            System.out.println("ERROR in calc.Calculator(treatment):" + ex);
        }
    }
}

