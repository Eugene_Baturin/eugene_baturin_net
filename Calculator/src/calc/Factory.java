package calc;

import java.io.IOException;
import java.util.Properties;

public class Factory {

    private static Factory instance;
    Properties properties = new Properties();

    private Factory(String configurationFile){
        try {
            properties.load(Factory.class.getResourceAsStream(configurationFile));
        } catch (IOException ex) {
            System.out.println("ERROR in Factory(constructor):" + ex);
        }
    }

    public static Factory getInstance(String configurationFile){
        try {
            if (instance==null) {
                instance = new Factory(configurationFile);
            }
            return instance;
        }catch (Exception ex) {
            System.out.println("ERROR in Factory(getInstance):" + ex);
            return null;
        }
    }

    Command get(String id){
        try {
            if (!(properties.contains(id))) {
                throw new Exception("Unknown command was typed");
            } else {
                Class c = Class.forName(properties.getProperty(id));
                Object o = c.newInstance();
                return (Command) o;
            }
        } catch (Exception ex) {
            System.out.println("ERROR in Factory(get):" + ex);
            return null;
        }
    }

}
