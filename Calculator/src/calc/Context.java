package calc;

import java.util.*;

public class Context {

    private Stack calcStack;
    private Map calcMap;

    public Context(){
        try{
            calcStack = new Stack<Double>();
            calcMap = new HashMap<String, Double>();
        }catch(Exception ex){
            System.out.println("ERROR in Context(constructor):" + ex);
        }
    }

    public Map<String,Double> getMap(){
        try{
            return calcMap;
        }catch(Exception ex){
            System.out.println("ERROR in Context(getMap):" + ex);
            return null;
        }
    }

    public Stack<Double> getStack(){
        try{
            return calcStack;
        }catch(Exception ex){
            System.out.println("ERROR in Context(getStack):" + ex);
            return null;
        }
    }

}
