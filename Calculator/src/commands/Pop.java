package commands;

import calc.*;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Pop implements Command {

    private static final Logger LOGGER = Logger.getLogger(Pop.class.getName());

    public void execute(List<String> curStr, Context curCont){
        try{
            if (curStr.size() == 1) {
                Double popValue = curCont.getStack().pop();
                LOGGER.log(Level.INFO, "POP executed successfully: the value " + popValue + " was deleted");
            }
            else throw new Exception("POP-command was typed incorrectly");
        }catch(Exception ex){
            LOGGER.log(Level.SEVERE, "Exception was caught in POP", ex);
            System.out.println("ERROR in commands.Pop:" + ex);
        }
    }
}
