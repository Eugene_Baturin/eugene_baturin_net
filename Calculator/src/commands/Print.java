package commands;

import calc.*;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Print implements Command {

    private static final Logger LOGGER = Logger.getLogger(Print.class.getName());

    public void execute(List<String> curStr, Context curCont){
        try{
            if (curStr.size() == 1) {
                System.out.println(curCont.getStack().peek());
                LOGGER.log(Level.INFO, "PRINT executed successfully: the value " + curCont.getStack().peek() + " was printed");
            }
            else throw new Exception("PRINT-command was typed incorrectly");
        }catch(Exception ex){
            LOGGER.log(Level.SEVERE, "Exception was caught in PRINT", ex);
            System.out.println("ERROR in commands.Print:" + ex);
        }
    }

}
