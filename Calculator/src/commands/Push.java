package commands;

import calc.*;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Push implements Command {

    private static final Logger LOGGER = Logger.getLogger(Push.class.getName());

    public void execute(List<String> curStr, Context curCont) {
        try{
            if ((curStr.size() == 2) && (curStr.get(1).matches("^[-]?[0-9]*[.,]?[0-9]+$"))) {
                curCont.getStack().push(Double.parseDouble(curStr.get(1)));
                LOGGER.log(Level.INFO, "PUSH-String executed successfully: the value " + curStr + " was pushed");
            }
            else if (curStr.size()== 2) {
                curCont.getStack().push(curCont.getMap().get(curStr.get(1)));
                LOGGER.log(Level.INFO, "PUSH-Double executed successfully: the value " + curCont.getMap().get(curStr) + " was pushed");
            }
            else throw new Exception("PUSH-command was typed incorrectly");
        }catch(Exception ex){
            LOGGER.log(Level.SEVERE, "Exception was caught in PUSH", ex);
            System.out.println("ERROR in commands.Push(String):" + ex);
        }
    }

}