
import calc.*;

import commands.Define;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DefineTest {

    @Test
    public void testDefine() {
        try {
            Context testCont = new Context();
            Define testClass = new Define();

            List<String> curStr1 = new ArrayList<>();
            curStr1.add("DEFINE");curStr1.add("a");curStr1.add("5");
            testClass.execute(curStr1, testCont);

            List<String> curStr2 = new ArrayList<>();
            curStr1.add("DEFINE");curStr2.add("B");curStr2.add("4");
            testClass.execute(curStr2, testCont);

            assertEquals(5.0, (double) testCont.getMap().get("a"));
            assertEquals(4.0, (double) testCont.getMap().get("B"));
        } catch (Exception ex) {
            System.out.println("ERROR in DefineTest:" + ex);
        }
    }
}