import calc.Context;
import commands.Pop;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PopTest {

    @Test
    public void testPop() {
        try {
            Context testCont = new Context();
            Pop testClass = new Pop();
            List<String> curStr1 = new ArrayList<>();
            curStr1.add("POP");

            testCont.getStack().push(5.0);
            testCont.getStack().push(6.0);
            testCont.getStack().push(7.0);
            int initialSize = testCont.getStack().size();
            testClass.execute(curStr1, testCont);
            testClass.execute(curStr1, testCont);
            int changedSize = testCont.getStack().size();

            assertEquals(2, initialSize-changedSize);
        } catch (Exception ex) {
            System.out.println("ERROR in PopTest:" + ex);
        }
    }
}