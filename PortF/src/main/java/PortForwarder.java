import org.xbill.DNS.ARecord;
import org.xbill.DNS.Message;
import org.xbill.DNS.Record;
import org.xbill.DNS.ResolverConfig;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.*;

public class PortForwarder {
    private static HashMap<Integer, SelectionKey> DNSMap = new HashMap<>();
    private String DNSServer = ResolverConfig.getCurrentConfig().server();
    private MessageManager MessageManager = new MessageManager();
    private static DatagramChannel DNSChannel;
    private Selector selector = null;
    private final int lport;

    public PortForwarder(final String args[]) {
        lport = Integer.valueOf(args[0]);
    }

    private void resolveDns() throws IOException {
        ByteBuffer buf = ByteBuffer.allocate(8192);

        if (DNSChannel.read(buf) > 0) {
            Message message = new Message(buf.array());
            Record[] records = message.getSectionArray(1);
            SelectionKey key = DNSMap.get(message.getHeader().getID());

            for (Record record : records) {
                if (record instanceof ARecord && key != null) {
                    Attachment attachment = (Attachment) key.attachment();
                    attachment.setHost(((ARecord) record).getAddress());
                    MessageManager.connectHost(attachment,
                            attachment.getHost(), attachment.getPort(), selector);
                    MessageManager.OkAnswerClient(attachment,
                            attachment.getHost(), (byte) attachment.getPort());
                    return;
                }
            }
        }
    }

    private void accept(SelectionKey key) throws IOException {
        SocketChannel clientChannel = ((ServerSocketChannel)key.channel()).accept();

        clientChannel.configureBlocking(false);
        clientChannel.register(selector, SelectionKey.OP_READ,
                new Attachment(clientChannel, selector));
    }

    private void connect(SelectionKey key) throws IOException {
        SocketChannel channel = ((SocketChannel) key.channel());
        Attachment attachment = ((Attachment) key.attachment());

        channel.finishConnect();
        attachment.deleteOption(SelectionKey.OP_CONNECT);
        attachment.addOption(SelectionKey.OP_WRITE);
    }

    private void read(final SelectionKey key) throws IOException {
        Attachment attachment = (Attachment) key.attachment();
        Attachment otherAttachment = attachment.getOtherAttachment();
        int byteRead = attachment.getSocketChannel().read(attachment.getBuf());

        if (attachment.getOtherAttachment() == null) {
            if(attachment.isFirstMessage()) {
                MessageManager.greetingMessage(attachment);
            } else {
                MessageManager.headersMessage(key, attachment,
                        byteRead, selector, DNSChannel, DNSMap);
            }
        } else {
            if (byteRead > 0 && otherAttachment.getSocketChannel().isConnected()) {
                otherAttachment.addOption(SelectionKey.OP_WRITE);
            }
            if (attachment.getBuf().position() == 0) {
                attachment.deleteOption(SelectionKey.OP_READ);
                otherAttachment.getSocketChannel().close();
            }
        }
    }

    private void write(final SelectionKey key) throws IOException {
        Attachment attachment = (Attachment) key.attachment();
        Attachment otherAttachment = attachment.getOtherAttachment();

        otherAttachment.getBuf().flip();

        int byteWrite = attachment.getSocketChannel().write(otherAttachment.getBuf());

        if (byteWrite > 0) {
            otherAttachment.getBuf().compact();
            otherAttachment.addOption(SelectionKey.OP_READ);
        }
        if(otherAttachment.getBuf().position() == 0) {
            attachment.deleteOption(SelectionKey.OP_WRITE);
        }
    }

    private void selectorInit() throws IOException {
        selector = Selector.open();
        ServerSocketChannel serverSocket = ServerSocketChannel.open();

        serverSocket.bind(new InetSocketAddress(lport));
        serverSocket.configureBlocking(false);
        serverSocket.register(selector, SelectionKey.OP_ACCEPT);
    }

    private SelectionKey dnsInit() throws IOException {
        DNSChannel = DatagramChannel.open();

        DNSChannel.configureBlocking(false);
        DNSChannel.connect(new InetSocketAddress(DNSServer, 53));
        return (DNSChannel.register(selector, SelectionKey.OP_READ));
    }

    public void run() throws IOException {
        selectorInit();

        SelectionKey DNSKey = dnsInit();

        while (selector.select() > -1) {
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                iterator.remove();
                if (key.isValid()) {
                    if (key == DNSKey && key.isReadable()) {
                        resolveDns();
                    } else if (key.isAcceptable()) {
                        System.out.println("Accept");
                        accept(key);
                    } else if (key.isConnectable()) {
                        System.out.println("Connect");
                        connect(key);
                    } else if (key.isReadable()) {
                        System.out.println("Read");
                        read(key);
                    } else if (key.isWritable()) {
                        System.out.println("Write");
                        write(key);
                    }
                }
            }
        }
    }
}
