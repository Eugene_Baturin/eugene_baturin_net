public class ExceptionType {
    public final static byte success = 0x00;
    public final static byte exceptionSocksServer = 0x01;
    public final static byte connectionBanned = 0x02;
    public final static byte networkNotAvailable = 0x03;
    public final static byte hostNotAvailable = 0x04;
    public final static byte connectionRefused = 0x05;
    public final static byte expiryTTL = 0x06;
    public final static byte commandNotSupported = 0x07;
    public final static byte addressTypeNotSupported = 0x08;
    public final static byte notDefined = 0x09;
}
