import org.xbill.DNS.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Map;

public class MessageManager {
    private static byte version = 0x05;

    public MessageManager() {

    }

    private void requestIPv4(final byte[] array, Attachment attachment,
                             final int byteRead, Selector selector) throws IOException {
        InetAddress host = getAddress(array);
        int port = getPort(array, byteRead);

        connectHost(attachment, host, port,selector);
        OkAnswerClient(attachment, host, (byte) port);
    }

    private void requestDomain(final byte[] array, Attachment attachment, final SelectionKey key,
                               DatagramChannel DNSChannel, Map DNSMap) throws IOException {
        int i, length = array[4];
        StringBuilder domain = new StringBuilder();

        for(i = 5; i < 5 + length; i++) {
            domain.append((char)array[i]);
        }

        attachment.setPort(((0xFF & array[i]) << 8) + (0xFF & array[i + 1]));

        Name name = Name.fromString(domain.toString(), Name.root);
        Record rec = Record.newRecord(name, Type.A, DClass.IN);
        Message msg = Message.newQuery(rec);

        DNSChannel.write(ByteBuffer.wrap(msg.toWire()));
        DNSMap.put(msg.getHeader().getID(), key);
    }

    public void headersMessage(SelectionKey key, Attachment attachment, final int byteRead,
                               Selector selector, DatagramChannel DNSChannel, Map DNSMap) throws IOException {
        byte[] array = attachment.getBuf().array();

        if(checkRequestIPV4(array)) {
            requestIPv4(array, attachment, byteRead, selector);
        } else if(checkRequestDOMAIN(array)) {
            requestDomain(array, attachment, key, DNSChannel, DNSMap);
        }
    }

    public void OkAnswerClient(Attachment attachment, InetAddress host,
                               final byte port) throws IOException {
        byte[] answer = {version,
                ExceptionType.success,
                0x00,
                AddressType.IPv4,
                host.getAddress()[0],
                host.getAddress()[1],
                host.getAddress()[2],
                host.getAddress()[3],
                0x00,
                port};
        attachment.getSocketChannel().write(ByteBuffer.wrap(answer));
        attachment.getBuf().clear();
    }

    public void connectHost(Attachment attachment, final InetAddress host,
                            final int port, Selector selector) throws IOException {
        SocketChannel newHostChannel = SocketChannel.open();

        newHostChannel.configureBlocking(false);
        newHostChannel.connect(new InetSocketAddress(host,port));

        Attachment newHost = new Attachment(newHostChannel, selector);

        attachment.setOtherAttachment(newHost);
        newHost.setOtherAttachment(attachment);
        attachment.getBuf().clear();
        attachment.getOtherAttachment().getBuf().clear();
        newHostChannel.register(selector, SelectionKey.OP_CONNECT | SelectionKey.OP_READ, newHost);
    }

    public void greetingMessage(Attachment attachment) throws IOException {
        if(checkGreeting(attachment.getBuf().array())) {
            byte[] answer = {version, AuthenticationType.noAuthenticationRequired};

            attachment.getSocketChannel().write(ByteBuffer.wrap(answer));
            attachment.getBuf().clear();
            attachment.setFirstMessage(false);
        } else {
            byte[] answer = {version, AuthenticationType.noMethods};

            attachment.getSocketChannel().write(ByteBuffer.wrap(answer));
            attachment.close();
        }
    }

    private InetAddress getAddress(final byte[] buf) throws UnknownHostException {
        return buf[3] == AddressType.IPv4 ?
                InetAddress.getByAddress(new byte[] {buf[4], buf[5], buf[6], buf[7]}) : null;
    }

    private boolean checkRequestIPV4(final byte[] buf) {
        return buf[0] == version
                && buf[1] == MethodType.connect
                && buf[3] == AddressType.IPv4;
    }

    private boolean checkRequestDOMAIN(final byte[] buf) {
        return buf[0] == version
                && buf[1] == MethodType.connect
                && buf[3] == AddressType.DOMAIN;
    }

    private boolean checkGreeting(final byte[] buf) {
        return buf[0] == version
                && buf[1] != 0
                && buf[2] == AuthenticationType.noAuthenticationRequired;
    }

    public int getPort(final byte[] buf, final int lenBuf) {
        return (((0xFF & buf[lenBuf - 2]) << 8) + (0xFF & buf[lenBuf - 1]));
    }
}
