import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

class Attachment {
    private Selector selector;
    private Attachment otherAttachment;
    private int bufferSize = 4096;
    private ByteBuffer buf = ByteBuffer.allocate(bufferSize);
    private SocketChannel socketChannel;
    private boolean firstMessage = true;
    private int port;

    public Attachment(final SocketChannel socketChannel, final Selector selector) {
        this.socketChannel = socketChannel;
        this.selector = selector;
    }

    Attachment getOtherAttachment() {
        return otherAttachment;
    }

    ByteBuffer getBuf() {
        return buf;
    }

    SocketChannel getSocketChannel() {
        return socketChannel;
    }

    void setOtherAttachment(Attachment otherAttachment) {
        this.otherAttachment = otherAttachment;
    }

    public InetAddress getHost() {
        return host;
    }

    public void setHost(InetAddress host) {
        this.host = host;
    }

    private InetAddress host;

    public boolean isFirstMessage() {
        return firstMessage;
    }

    public void setFirstMessage(boolean firstMessage) {
        this.firstMessage = firstMessage;
    }

    void addOption(int option){
        SelectionKey currentOption = socketChannel.keyFor(selector);
        currentOption.interestOps(currentOption.interestOps()|option);
    }

    void deleteOption(int option){
        SelectionKey currentOption = socketChannel.keyFor(selector);
        currentOption.interestOps(currentOption.interestOps()&~option);
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    void close() {
        try {
            socketChannel.close();
            socketChannel.keyFor(selector).cancel();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}





