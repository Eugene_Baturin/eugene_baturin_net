public class AuthenticationType {
    public final static byte noAuthenticationRequired = 0x00;
    public final static byte noMethods = (byte) 0xFF;
}
