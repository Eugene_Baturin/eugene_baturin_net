import io.undertow.websockets.core.WebSocketChannel;

import java.util.List;
import java.util.Map;

public interface WSHandler {

    public boolean handle(final String message, Map<Integer, UserInfo> nameUsers);

}