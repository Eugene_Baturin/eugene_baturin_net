import com.google.gson.Gson;

import java.util.List;
import java.util.Map;

public class MessageHandler implements WSHandler {
    private final int SERVER_ID = -1;
    private final int LOGINNING = -1;
    private final int LOGOUTING = -2;
    private final int BYE_MESSAGE = -3;

    public MessageHandler() {
        super();
    }

    private void connecting(final Message messObj, Map<Integer, UserInfo> nameUsers) {
        UserInfo newUser = (new Gson()).fromJson(messObj.getMessage(), UserInfo.class);

        if (nameUsers.containsKey(newUser.getId())) {
            nameUsers.get(newUser.getId()).toOnline();
        } else {
            nameUsers.put(newUser.getId(), newUser);
        }
        System.out.println("Notification: " + newUser.getName() + " has been connected");
    }

    private void detaching(final Message messObj, Map<Integer, UserInfo> nameUsers) {
        UserInfo detachedUser = (new Gson()).
                fromJson(messObj.getMessage(), UserInfo.class);

        nameUsers.get(detachedUser.getId()).toOffline();
        System.out.println("Notification: " + detachedUser.getName() + " has been detached");
    }

    private void serverMessagesFork(final Message messObj,
                                    final Map<Integer, UserInfo> nameUsers) {
        if (messObj.getId() == LOGINNING) {
            connecting(messObj, nameUsers);
        } else if (messObj.getId() == LOGOUTING) {
            detaching(messObj, nameUsers);
        } else if (messObj.getId() == BYE_MESSAGE) {
            System.out.println("Server: " + messObj.getMessage());
        }
    }

    @Override
    public boolean handle(final String message, Map<Integer, UserInfo> nameUsers) {
        Message messObj = (new Gson()).fromJson(message, Message.class);

        if (messObj.getAuthor() != Identity.INSTANCE.userInfo.getId()) {
            if (messObj.getAuthor() == SERVER_ID) {
                serverMessagesFork(messObj, nameUsers);
            } else {
                System.out.println(nameUsers.get(messObj.getAuthor()).getName() + ": " + messObj.getMessage());
            }
        }

        return true;
    }
}