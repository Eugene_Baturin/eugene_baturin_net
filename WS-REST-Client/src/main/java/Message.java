
public class Message {
    private int id;
    private String message;
    private int author;

    public Message(final int id, final String message, final int author) {
        this.id = id;
        this.message = message;
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public int getAuthor() {
        return author;
    }

    public String getMessage() {
        return message;
    }
}
