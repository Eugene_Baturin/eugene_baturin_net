import com.google.gson.Gson;

import java.util.Map;
import java.util.Scanner;

public class WebSocketHandler {
    private final ChatClientEndPoint clientEndPoint;
    private Map<Integer, UserInfo> nameUsers;
    private Factory WSFactory;
    private boolean isExitRequest = false;
    private boolean isIdentityDetermined = false;
    private boolean isUsersReceived = false;
    private boolean isMessagesReceived = false;

    public WebSocketHandler(final ChatClientEndPoint clientEndPoint, final Map nameUsers) {
        this.clientEndPoint = clientEndPoint;
        this.nameUsers = nameUsers;
        this.WSFactory = Factory.getInstance();
    }

    private boolean callHandler(final String message, final String handlerName) {
        WSHandler cmd = WSFactory.get(handlerName);

        if (cmd != null) {
            return cmd.handle(message, nameUsers);
        }

        return false;
    }

    private void handlersFork(final String message) {
        if (!isIdentityDetermined) {
            isIdentityDetermined = callHandler(message, "LoginHandler");
        } else if (!isUsersReceived) {
            isUsersReceived = callHandler(message, "UsersHandler");
        } else if (!isMessagesReceived) {
            isMessagesReceived = callHandler(message, "MessagesHandler");
        }
    }

    public void addIncomingMessagesHandler() {
        clientEndPoint.addMessageHandler(new ChatClientEndPoint.MessageHandler() {
            public void handleMessage(String message) {
                if (!(isIdentityDetermined && isUsersReceived && isMessagesReceived)) {
                    handlersFork(message);
                } else {
                    callHandler(message, "MessageHandler");
                }
            }
        });
    }

    class MessageContent {
        public String message;

        public MessageContent(final String message) {
            this.message = message;
        }
    }

    class UserName {
        public String username;

        public UserName(final String username) {
            this.username = username;
        }
    }

    private void loginning(Scanner scanner) {
        System.out.print("Login as: ");
        String string = new String(scanner.nextLine());
        clientEndPoint.sendMessage((new Gson()).toJson(new UserName(string)));
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void messaging(Scanner scanner) {
        String string = new String(scanner.nextLine());
        if (string.equals("/logout")) {
            isExitRequest = true;
        }
        clientEndPoint.sendMessage((new Gson()).toJson(new MessageContent(string)));
    }

    public void addOutcomingMessagesHandler() {
        Scanner scanner = new Scanner(System.in);
        while (!(isIdentityDetermined && isUsersReceived && isMessagesReceived)) {
            loginning(scanner);
        }
        while (!isExitRequest) {
            messaging(scanner);
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
