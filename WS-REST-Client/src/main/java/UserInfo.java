import java.io.Serializable;
import java.util.UUID;

public class UserInfo implements Serializable {
    private final int id;
    private final String name;
    private boolean online;

    public UserInfo(final int id, final String name, final boolean online) {
        this.id = id;
        this.name = name;
        this.online = online;
    }

    public void toOnline() {
        this.online = true;
    }

    public void toOffline() {
        this.online = false;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isOnline() {
        return online;
    }
}
