import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

public class ChatBot {

    private static Map<Integer, UserInfo> nameUsers;

    static {
        nameUsers = new ConcurrentHashMap<>();
    }

    public static void main(String[] args) throws Exception {
        final ChatClientEndPoint clientEndPoint = new ChatClientEndPoint(new URI("ws://localhost:8080/chat"));
        WebSocketHandler handler = new WebSocketHandler(clientEndPoint, nameUsers);

        handler.addIncomingMessagesHandler();
        handler.addOutcomingMessagesHandler();
    }
}