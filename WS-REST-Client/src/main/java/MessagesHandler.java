import com.google.gson.Gson;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;

import java.util.List;
import java.util.Map;

public class MessagesHandler implements WSHandler {

    public MessagesHandler() {
        super();
    }

    class MessagesDto {
        public List<Message> messages;

        public MessagesDto(List<Message> messages) {
            this.messages = messages;
        }
    }

    @Override
    public boolean handle(final String message, Map<Integer, UserInfo> nameUsers) {
        MessagesDto messList = (new Gson()).fromJson(message, MessagesDto.class);

        for (Message mess: messList.messages) {
            System.out.println(nameUsers.get(mess.getAuthor()).getName() + ": " + mess.getMessage());
        }

        return true;
    }
}
