import com.google.gson.Gson;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class LoginHandler implements WSHandler {
    private final int ALREADY_IN_USE = -1;

    public LoginHandler() {
        super();
    }

    private void identification(final Map<Integer, UserInfo> nameUsers,
                                final UserInfo userInfo) {
        Identity passport = Identity.INSTANCE;

        passport.setInstance(userInfo);
        nameUsers.put(userInfo.getId(), userInfo);
    }

    @Override
    public boolean handle(final String message,
                          final Map<Integer, UserInfo> nameUsers) {
        UserInfo userInfo = (new Gson()).fromJson(message, UserInfo.class);

        if (userInfo.getId() == ALREADY_IN_USE) {
            System.out.println("Username is already in use");
            return false;
        } else {
            identification(nameUsers, userInfo);
        }

        return true;
    }
}