import com.google.gson.Gson;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;

import java.util.List;
import java.util.Map;

public class UsersHandler implements WSHandler {

    public UsersHandler() {
        super();
    }

    class MessageContent {
        public int id;
        public String message;
        public int author;
    }

    class UsersDto {
        public List<UserInfo> users;

        public UsersDto(List<UserInfo> users) {
            this.users = users;
        }
    }

    @Override
    public boolean handle(final String message, Map<Integer, UserInfo> nameUsers) {
        UsersDto usersList = (new Gson()).fromJson(message, UsersDto.class);

        for (UserInfo user: usersList.users) {
            nameUsers.put(user.getId(), user);
        }

        return true;
    }
}
