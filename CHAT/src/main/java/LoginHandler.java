import io.undertow.io.IoCallback;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


class LoginHandler implements HttpHandler {
    private static Set<User> users;

    public LoginHandler() {
        super();
    }

    static {
        users = new HashSet<>();
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws IOException {
        exchange.getRequestReceiver().receiveFullString((HttpServerExchange excng, String bytes) -> {
            JsonObject jsonObject = new JsonParser().parse(bytes).getAsJsonObject();
            String userName = jsonObject.get("username").getAsString();
            User newUser = new User(1, userName, true, UUID.randomUUID());
            users.add(newUser);
            exchange.getResponseSender().send(newUser.toJson().toString(), IoCallback.END_EXCHANGE);
            /*if (users.contains(newUser)) {

            } else {
                users.add(newUser);
            }*/
        });
    }
}