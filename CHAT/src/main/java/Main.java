import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

public class Main {
    private static Users users = new Users();
    private static Messages messages = new Messages();

    public static void main(final String[] args) {
        Undertow server = Undertow.builder()
                .addHttpListener(8080, "localhost")
                .setHandler(
                        Handlers.path()
                                .addPrefixPath("/login", new LoginHandler())
                                .addPrefixPath("/logout", new LogoutHandler())
                                //.addPrefixPath("/users", new UserHandler(users))
                                //.addPrefixPath("/messages", new MessagesHandler(users,messages))
                                //.addPrefixPath("/", new ErrorHandler())
                ).build();
        server.start();
    }
}
