import com.google.gson.JsonObject;

import java.util.UUID;

public class User {
    private final int id;
    private final String name;
    private boolean online;
    private final UUID token;

    public User(final int id, final String name, final boolean online, final UUID token) {
        this.id = id;
        this.name = name;
        this.online = online;
        this.token = token;
    }

    public JsonObject toJson(){
        JsonObject jsonObject = new JsonObject().getAsJsonObject("{\"id\": " + this.id + "," +
                "\"username\": " + this.name + "," + "\"online\": " + this.online + "," +
                "\"token\": " + this.token.toString() + "}");
        return jsonObject;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public UUID getToken() {
        return token;
    }

    public boolean getOnline() {
        return online;
    }
}
