import java.security.Principal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import io.undertow.security.idm.Account;
import io.undertow.security.idm.Credential;
import io.undertow.security.idm.IdentityManager;
import io.undertow.security.idm.PasswordCredential;

public class CustomIdentityManager implements IdentityManager {

    private final Set<String> users;

    CustomIdentityManager(final Set<String> users) {
        this.users = users;
    }

    @Override
    public Account verify(Account account) {
        return account;
    }

    @Override
    public Account verify(Credential credential) {
        return null;
    }

    @Override
    public Account verify(String id, Credential credential) {
        Account account = getAccount(id);
        if (account != null && verifyCredential(account, credential)) {
            return account;
        }
        return null;
    }

    private boolean verifyCredential(Account account, Credential credential) {

        return true;
    }

    private Account getAccount(final String id) {
        if (users.contains(id)) {
            return new Account() {
                private static final long serialVersionUID = 1L;

                private final Principal principal = () -> id;

                @Override
                public Principal getPrincipal() {
                    return principal;
                }

                @Override
                public Set<String> getRoles() {
                    return Collections.emptySet();
                }
            };
        }
        return null;
    }

}