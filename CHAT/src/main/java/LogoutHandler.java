import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.HttpString;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class LogoutHandler implements HttpHandler {
    private static Set<String> users;

    public LogoutHandler() {
        super();
    }

    static {
        users = new HashSet<>();
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws IOException {
        exchange.getRequestReceiver().receiveFullString((HttpServerExchange excng, String bytes) -> {
            JsonObject jsonObject = new JsonParser().parse(bytes).getAsJsonObject();
            String pageName = jsonObject.get("username").getAsString();
            if (users.contains(pageName)) {

            } else {
                users.add(pageName);
            }
        });
    }
}
