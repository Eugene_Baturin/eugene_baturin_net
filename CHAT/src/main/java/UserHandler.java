import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;

public class UserHandler implements HttpHandler{

    private final HttpString header;
    private final String value;
    private final HttpHandler next;

    public UserHandler(final HttpHandler next, final String header, final String value) {
        this.next = next;
        this.header = new HttpString(header);
        this.value = value;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        exchange.getResponseHeaders().put(header, value);
        System.out.println(value);
        next.handleRequest(exchange);
    }
}
